var PUZZLES = {
	"easy": [
		[
			{
				"name": "lamp",
				"data": "081c1c3e0808081c",
				"fg": "#fea",
				"bg": "#542"
			},
			{
				"name": "top hat",
				"data": "003c3c3c3c3c7e00",
				"fg": "#222",
				"bg": "#aad"
			},
			{
				"name": "banjo", //e
				"data": "181808081c2a2a1c",
				"fg": "#321",
				"bg": "#fed"
			},
			{
				"name": "dollar", //e
				"data": "083e0a3e283e0800",
				"fg": "#282",
				"bg": "#eed"
			},
			{
				"name": "tank", //e
				"data": "00383f387effff7e",
				"fg": "#271",
				"bg": "#ccc"
			},
			{
				"name": "sigma", //easy
				"data": "7f460c180c467f00",
				"fg": "#522",
				"bg": "#eee"
			},
			{
				"name": "plunger", //e
				"data": "0808080808081c3e",
				"fg": "#542",
				"bg": "#fea"
			},
			{
				"name": "battery", //e
				"data": "187e42424242427e",
				"fg": "#fd4",
				"bg": "#222"
			}
		],
		[
			{
				"name": "ghost", //e
				"data": "e7a58181c3e3e3c7",
				"fg": "#416",
				"bg": "#feb"
			},
			{
				"name": "letter", //e
				"data": "00ff81e79981ff00",
				"fg": "#222",
				"bg": "#eee"
			},
			{
				"name": "anchor", //e
				"data": "083e0808086b2a1c",
				"fg": "#444",
				"bg": "#aaf"
			},
			{
				"name": "mug", //e
				"data": "001e7e5e7e1e1e00",
				"fg": "#222",
				"bg": "#6cc"
			},
			{
				"name": "heart", //e
				"data": "367d7f6f361c0800",
				"fg": "#a13",
				"bg": "#fdd"
			},
			{
				"name": "moon", //e
				"data": "dfbf3f3f1f0e81c3",
				"fg": "#228",
				"bg": "#fe6"
			},
			{
				"name": "fire hydrant", //easy
				"data": "081c1c3e1c3c1c1c",
				"fg": "#822",
				"bg": "#bbb"
			},
			{
				"name": "wine glass", //e
				"data": "3e223e1c0808143e",
				"fg": "#913",
				"bg": "#eee"
			}
		],
		[
			{
				"name": "maple leaf", //easy
				"data": "18dbffffff7e0808",
				"fg": "#e22",
				"bg": "#eee"
			},
			{
				"name": "USA", //e
				"data": "0000bfdf7f7e4800",
				"fg": "#371",
				"bg": "#bbf"
			},
			{
				"name": "bowling ball", //e
				"data": "3c7ed7ffffef7e3c",
				"fg": "#222",
				"bg": "#fda"
			},
			{
				"name": "flying rodent man", //e
				"data": "223e3e2a3e2a221c",
				"fg": "#114",
				"bg": "#987"
			},
			{
				"name": "gyroscope", //e
				"data": "081c2a7f2a1c0800",
				"fg": "#444",
				"bg": "#ece"
			},
			{
				"name": "rake", //e
				"data": "080808081c2a7f55",
				"fg": "#543",
				"bg": "#ac6"
			},
			{
				"name": "teddy bear", //e
				"data": "363e1c7f7f1c7f77",
				"fg": "#642",
				"bg": "#ddf"
			},
			{
				"name": "clothes hanger", //e
				"data": "e7ebeff7ebddbe80",
				"fg": "#432",
				"bg": "#ccc"
			}
		],
		[
			{
				"name": "pill", //e
				"data": "003e49497f3e0000",
				"fg": "#711",
				"bg": "#fd8"
			},
			{
				"name": "dumbbell", //e
				"data": "004242ff42420000",
				"fg": "#222",
				"bg": "#f55"
			},
			{
				"name": "US flag", //e
				"data": "f50af50aff00ff00",
				"image": "images/puzzle/usflag.png",
				"fg": "#600",
				"bg": "#ccf"
			},
			{
				"name": "outlet", //n, e
				"data": "3e4155554149413e",
				"fg": "#888",
				"bg": "#ffd"
			},
			{
				"name": "folder", //e
				"data": "0ef18181818181ff",
				"fg": "#432",
				"bg": "#fda"
			},
			{
				"name": "tombstone", //e
				"data": "e7c3c38181818181",
				"fg": "#224",
				"bg": "#888"
			},
			{
				"name": "piano", //e
				"data": "ffff9595950000ff",
				"fg": "#111",
				"bg": "#eed"
			},
			{
				"name": "vase", //e
				"data": "3c183c7e7e3c3c18",
				"fg": "#362",
				"bg": "#dcb"
			}
		],
		[
			{
				"name": "egg", //e
				"data": "ffe7c3c38181c3ff",
				"fg": "#541",
				"bg": "#eee"
			},
			{
				"name": "pi", //e
				"data": "feff353436363666",
				"fg": "#131",
				"bg": "#cfc"
			},
			{
				"name": "book", //e
				"data": "7c667e667e7e027c",
				"fg": "#630",
				"bg": "#eed"
			},
			{
				"name": "potted flower", //e
				"data": "2a3e1c08083e1c1c",
				"fg": "#952",
				"bg": "#ddf"
			},
			{
				"name": "snowman", //e
				"data": "e7c3e7c3c38181c3",
				"fg": "#22a",
				"bg": "#eee"
			},
			{
				"name": "flag", //e
				"data": "020e3efe1e020202",
				"fg": "#390",
				"bg": "#bcf"
			},
			{
				"name": "magic lamp", //e, n
				"data": "0010fbbe78103800",
				"fg": "#a73",
				"bg": "#ecf"
			},
			{
				"name": "microscope", //e, n
				"data": "041c3464606e6cff",
				"fg": "#222",
				"bg": "#aba"
			}
		],
		[
			{
				"name": "toaster", //e, n
				"data": "3e55415d4949417f",
				"fg": "#322",
				"bg": "#aaa"
			},
			{
				"name": "high heel", //e, n
				"data": "0000061e3a720000",
				"fg": "#321",
				"bg": "#fcf"
			},
			{
				"name": "giraffe", //e, n
				"data": "206020203c3a2828",
				"fg": "#420",
				"bg": "#ccf"
			},
			{
				"name": "trunk", //e, n
				"data": "7ec399ff998181ff",
				"fg": "#321",
				"bg": "#b97"
			},
			{
				"name": "bat", //e, n
				"data": "00a9bbffff7e2800",
				"fg": "#222",
				"bg": "#88a"
			},
			{
				"name": "rattle", //e, n
				"data": "1c3e221c081c141c",
				"fg": "#494",
				"bg": "#fea"
			},
			{
				"name": "airplane", //e, m
				"data": "0008081c3e6b081c",
				"fg": "#445",
				"bg": "#bbf"
			},
			{
				"name": "camera", //e, n
				"data": "007052ffe7e7ff00",
				"fg": "#444",
				"bg": "#fe4"
			}
		],
		[
			{
				"name": "boot", //e, n
				"data": "00f87070787e6e00",
				"fg": "#420",
				"bg": "#bda"
			},
			{
				"name": "pear", //e
				"data": "18081c1c3e7f7f3e",
				"fg": "#4c2",
				"bg": "#fe4"
			},
			{
				"name": "skull", //e, n
				"data": "e3c1d5c9e3ffe3ff",
				"fg": "#222",
				"bg": "#edc"
			},
			{
				"name": "cursor", //e, n
				"data": "02060e1e3e0a1010",
				"fg": "#222",
				"bg": "#eee"
			},
			{
				"name": "stairs", //e,n
				"data": "c040f010fc04ff01",
				"fg": "#642",
				"bg": "#edb"
			},
			{
				"name": "castle", //e, n
				"data": "00a5e77e7e6666ff",
				"fg": "#444",
				"bg": "#dde"
			},
			{
				"name": "spanner", //n, e
				"data": "00060ffcbfe60000",
				"fg": "#333",
				"bg": "#cdc"
			},
			{
				"name": "sailboat", //e, n
				"data": "08183878f808ff7e",
				"fg": "#411",
				"bg": "#adf"
			}
		],
		[
			{
				"name": "kite", //en
				"data": "0f0f0f0f18306040",
				"fg": "#a63",
				"bg": "#bcf"
			},
			{
				"name": "car", //n, e
				"data": "00003874ffff6600",
				"fg": "#228",
				"bg": "#aaa"
			},
			{
				"name": "umbrella", //n, e
				"data": "083e7f7f49080818",
				"fg": "#511",
				"bg": "#88a"
			},
			{
				"name": "butcher knife", //e
				"data": "0000ff1d1f1f0000",
				"fg": "#222",
				"bg": "#edc"
			},
			{
				"name": "lipstick", //e
				"data": "1018183c2424243c",
				"fg": "#a11",
				"bg": "#dcc"
			},
			{
				"name": "pickup", //normal, e
				"data": "001814ffff666600",
				"fg": "#222",
				"bg": "#8b8"
			},
			{
				"name": "goal post", //e
				"data": "4242427e18181818",
				"fg": "#444",
				"bg": "#89f"
			},
			{
				"name": "trophy", //e  (final easy)
				"data": "3cffbdbd7e183c7e",
				"fg": "#841",
				"bg": "#ee2"
			}
		],
		/* easy set 2 */
		[
			{
				"name":"brick", //e
				"data":"777700dddd007777",
				"fg": "#822",
				"bg": "#aaa"
			},
			{
				"name":"dreidel", //e
				"data":"0808083e3e3e1c08",
				"fg": "#631",
				"bg": "#7cf"
			},
			{
				"name":"blender", //e, n
				"data":"3cfca4a4e43c7e7e",
				"fg": "#800",
				"bg": "#ddd"
			},
			{
				"name":"road", //e
				"data":"77777f77777f7777",
				"fg": "#111",
				"bg": "#fd2"
			},
			{
				"name":"axe", //e
				"data":"103e3e1610101010",
				"fg": "#222",
				"bg": "#6b4"
			},
			{
				"name":"pogo stick", //e
				"data":"003e0808081c0808",
				"fg": "#900",
				"bg": "#aaa"
			},
			{
				"data":"003c7ee7c3ffff00", //e
				"name":"protractor",
				"fg": "#dde",
				"bg": "#7af"
			},
			{
				"name":"film", //e
				"data":"ff45c77dc745ff45",
				"fg": "#111",
				"bg": "#aaa"
			}
		],
		[
			{
				"name":"mushroom", //e
				"data":"1c3e7f7f1414141c",
				"fg": "#515",
				"bg": "#fda"
			},
			{
				"data":"85ff8195ff81a1ff", //e, n
				"name":"abacus",
				"fg": "#322",
				"bg": "#acf"
			},
			{
				"data":"3e1210386c447c7c", //n, e
				"name":"hand soap",
				"fg": "#3a6",
				"bg": "#eee"
			},
			{
				"data":"3c24ff81ff9981ff", //e
				"name":"toolbox",
				"fg": "#222",
				"bg": "#e44"
			},
			{
				"name":"cupcake", //e, n
				"data":"1866ffff7e42423c",
				"fg": "#421",
				"bg": "#faa"
			},
			{
				"name":"window", //e
				"data":"7e42427e4242ff7e",
				"fg": "#432",
				"bg": "#8cf"
			},
			{
				"name":"cane", //e
				"data":"1c3e363030303030",
				"fg": "#642",
				"bg": "#fda"
			},
			{
				"name":"phi", //e
				"data":"1c083e6b6b3e081c",
				"fg": "#272",
				"bg": "#ccc"
			}
		],
		[
			{
				"data":"7557722775577227", //e
				"name":"chains",
				"fg": "#666",
				"bg": "#8af"
			},
			{
				"name":"sythe", //n, e
				"data":"787e232020202020",
				"fg": "#222",
				"bg": "#96b"
			},
			{
				"data":"007f41c181ff6600", //e
				"name":"school bus",
				"fg": "#322",
				"bg": "#fd2"
			},
			{
				"data":"000c683e1c081c00", //e
				"name":"eagle",
				"fg": "#543",
				"bg": "#4af"
			},
			{
				"data":"3c340c1c040c180c", //e
				"name":"snake",
				"fg": "#260",
				"bg": "#ccc"
			},
			{
				"data":"1c0c1c3c7c1c3c3c", //e
				"name":"moai",
				"fg": "#777",
				"bg": "#8e4"
			},
			{
				"data":"7050547e1212121e", //e
				"name":"lighter",
				"fg": "#422",
				"bg": "#eee"
			},
			{
				"data":"36223e3e3e3e3e3e", //e
				"name":"clipboard",
				"fg": "#641",
				"bg": "#ccc"
			}
		],
		[
			{
				"data":"ff8100000081cf9f", //e
				"name":"speech bubble",
				"fg": "#336",
				"bg": "#eee"
			},
			{
				"data":"ff81818181ff183c", //e
				"name":"monitor",
				"fg": "#222",
				"bg": "#6af"
			},
			{
				"data":"7e5a427e5a42427e", //e
				"name":"file cabinet",
				"fg": "#444",
				"bg": "#fda"
			},
			{
				"name":"ping pong", //e
				"data":"6060000e1fff1f0e",
				"fg": "#222",
				"bg": "#3e6"
			},
			{
				"data":"3e1c1c1c7f080808", //e
				"name":"thumb tack",
				"fg": "#384",
				"bg": "#edb"
			},
			{
				"data":"00feff437f03ff00", //e
				"name":"stapler",
				"fg": "#800",
				"bg": "#eee"
			},
			{
				"data":"3e143e143e141c08", //e
				"image": "images/puzzle/trafficlights.png",
				"name":"traffic lights",
				"fg": "#543",
				"bg": "#4e4"
			},
			{
				"data":"00018187ffff8100", //e
				"name":"bed",
				"fg": "#752",
				"bg": "#aaa"
			}
		],
		[
			{
				"data":"081c1c08083e3e00", //e
				"name":"stamp",
				"fg": "#722",
				"bg": "#eee"
			},
			{
				"data":"00ff01011155ff00", //e
				"name":"ruler",
				"fg": "#531",
				"bg": "#fd8"
			},
			{
				"name":"ladder", //e
				"data":"00243c243c243c24",
				"fg": "#642",
				"bg": "#8af"
			},
			{
				"data":"0074fffff87c6c00", //e
				"name":"hippopotamus",
				"fg": "#444",
				"bg": "#6a4"
			},
			{
				"data":"e0e6e66060202070", //e
				"name":"baseball",
				"fg": "#876",
				"bg": "#5e6"
			},
			{
				"data":"3e22e3bfffffe33e", //n, e
				"name":"printer",
				"fg": "#222",
				"bg": "#edc"
			},
			{
				"data":"00e0bda7bde00000", //n, e
				"name":"telescope",
				"fg": "#543",
				"bg": "#cb5"
			},
			{
				"data":"7e427e6a566a567e", //n, e
				"name":"calculator",
				"fg": "#226",
				"bg": "#eee"
			}
		],
		[
			{
				"data":"2424247e7e3c1818", //n, e
				"name":"power plug",
				"fg": "#444",
				"bg": "#fd4"
			},
			{
				"data":"fdf7fffd20a0e080", //n, e
				"name":"security camera",
				"fg": "#222",
				"bg": "#c99"
			},
			{
				"data":"92baba92d2d79792", //e, n
				"name":"mixer board",
				"fg": "#522",
				"bg": "#fda"
			},
			{
				"name":"eye dropper", //e
				"data":"081c1c3e14141408",
				"fg": "#226",
				"bg": "#ccc"
			},
			{
				"name":"toilet", //e
				"data":"e06060607f3e183c",
				"fg": "#eee",
				"bg": "#864"
			},
			{
				"name":"swing", //e
				"data":"ffa5a5a5a5bd9981",
				"fg": "#422",
				"bg": "#59f"
			},
			{
				"name":"tape measure", //e
				"data":"00000f0d0fff8000",
				"fg": "#333",
				"bg": "#fc4"
			},
			{
				"name":"pawn", //e
				"data":"18343c187e3c7e7e",
				"fg": "#222",
				"bg": "#ccc"
			}
		],
		[
			{
				/* Jacob Cappell */
				"name": "taxi", //e
				"data": "18187e42ffbdff81",
				"fg": "#ba1",
				"bg": "#ddd"
			},
			{
				"data":"7c737c3030307c7c", //n, e
				"name":"electric drill",
				"fg": "#229",
				"bg": "#bbb"
			},
			{
				"name":"waffle", //e
				"data":"3e63554155633e00",
				"fg": "#631",
				"bg": "#fc8"
			},
			{
				"name":"spatula", //e, n
				"data":"3e2a2a1c080c0c0c",
				"fg": "#333",
				"bg": "#69d"
			},
			{
				"name":"memory card", //e, n
				"data":"7e55557f7f7f7f7f",
				"fg": "#225",
				"bg": "#fd3"
			},
			{
				"name":"ink bottle", //e, n
				"data":"3c3c187efdffffff",
				"fg": "#222",
				"bg": "#aaf"
			},
			{
				"name":"cutlass", //e, n
				"data":"10181818103c1418",
				"fg": "#333",
				"bg": "#bcf"
			},
			{
				"data":"0000fcfed57f0000", //n, e
				"name":"harmonica",
				"fg": "#321",
				"bg": "#abd"
			}
		],
		[
			{
				"name":"scales", //n, e
				"data":"083e08086b08081c",
				"fg": "#741",
				"bg": "#eee"
			},
			{
				"data":"1c777f3a3a3a3a3e", //n, e
				"name":"trash can",
				"fg": "#333",
				"bg": "#aaa"
			},
			{
				"data":"60604060427e1018", //n, e
				"name":"russian dance",
				"fg": "#222",
				"bg": "#e22"
			},
			{
				"data":"1824247ea5a5a57e", //n, e
				"name":"duffle bag",
				"fg": "#531",
				"bg": "#ccc"
			},
			{
				"data":"386c445454545c18", //n, e
				"name":"paper clip",
				"fg": "#888",
				"bg": "#fed"
			},
			{
				"data":"18dbff6666ffdb18", //n, e
				"name":"gear",
				"fg": "#752",
				"bg": "#ccc"
			},
			{
				"data":"0808fffbb1dbff00", //n, e
				"name":"game pad",
				"fg": "#a22",
				"bg": "#eee"
			},
			{
				"name":"blue ribbon", //e, n  last
				"data":"1c3622361c1c1c14",
				"fg": "#00a",
				"bg": "#ffd"
			}
		]
	],
	"normal": [
		[
			{
				"name": "four-leaf clover", //n
				"data": "36777f1c7f773660",
				"fg": "#371",
				"bg": "#ddb"
			},
			{
				"name": "snail", //n
				"data": "a0e04e5151517e3f",
				"fg": "#620",
				"bg": "#fed"
			},
			{
				"name": "yin-yang", //normal?
				"data": "3c46d7879fd75e3c",
				"fg": "#222",
				"bg": "#eee"
			},
			{
				"name": "dog", //n
				"data": "20e0e03d3f3c246c",
				"fg": "#543",
				"bg": "#9af"
			},
			{
				"name": "cactus", //n
				"data": "18181a5a5e781818",
				"fg": "#271",
				"bg": "#fd4"
			},
			{
				"name": "rocket", //n
				"data": "081c14141436771c",
				"fg": "#444",
				"bg": "#cdf"
			},
			{
				"name": "moose", //n
				"data": "40507018feefe6c0",
				"fg": "#421",
				"bg": "#cfc"
			},
			{
				"name": "palm tree", //n
				"data": "6cfe9ab210101818",
				"fg": "#460",
				"bg": "#bcf"
			}
		],
		[
			{
				"name": "rook", //n
				"data": "2a3e3e1c1c1c3a3e",
				"fg": "#222",
				"bg": "#ccc"
			},
			{
				"name": "silverware", //n
				"data": "55d5dfce44444444",
				"fg": "#333",
				"bg": "#fed"
			},
			{
				"name": "notes", //n
				"data": "1ce49ce48487e7e0",
				"fg": "#222",
				"bg": "#ddc"
			},
			{
				"name": "signal", //n
				"data": "7eff813c7e421818",
				"fg": "#446",
				"bg": "#aca"
			},
			{
				"name": "ice cream cone", //n
				"data": "3e63416f2e1c1c08",
				"fg": "#530",
				"bg": "#fea"
			},
			{
				"name": "owl", //n
				"data": "223e36143e36221c",
				"image": "images/puzzle/owl.png",
				"fg": "#320",
				"bg": "#99b"
			},
			{
				"name": "key", //n
				"data": "0060f0bff5600000",
				"fg": "#851",
				"bg": "#aca"
			},
			{
				"name": "lock", //n
				"data": "1824247e627e627e",
				"fg": "#432",
				"bg": "#ccd"
			}
		],
		[
			{
				"name": "fire helmet", //normal
				"data": "001c3e3232ffc000",
				"fg": "#911",
				"bg": "#ed4"
			},
			{
				"name": "seahorse", //normal
				"data": "103c301818302418",
				"fg": "#a37",
				"bg": "#acf"
			},
			{
				"name": "purse", //n
				"data": "18247e8199c3ff7e",
				"fg": "#846",
				"bg": "#fdb"
			},
			{
				"name": "pliers", //n
				"data": "36223e1c36636336",
				"fg": "#222",
				"bg": "#d33"
			},
			{
				"name": "television", //normal
				"data": "ddebf7005e1e5e00",
				"fg": "#447",
				"bg": "#ddd"
			},
			{
				"name": "leafy flower", //n
				"data": "2a3e1c08086b3e0c",
				"fg": "#494",
				"bg": "#fcf"
			},
			{
				"name": "hammer", //n
				"data": "7afeba1028282838",
				"fg": "#320",
				"bg": "#ba9"
			},
			{
				"name": "shiny ball", //n
				"data": "3c66e7ffffbd423c",
				"fg": "#222",
				"bg": "#bef"
			}
		],
		[
			{
				"name": "proposal", //n
				"data": "0c0c68380838286e",
				"fg": "#222",
				"bg": "#fb2"
			},
			{
				"name": "house", //n
				"data": "00143c7cfe7c5474",
				"fg": "#632",
				"bg": "#8ac"
			},
			{
				"name": "question mark", //n
				"data": "3c66607818001818",
				"fg": "#431",
				"bg": "#fe4"
			},
			{
				"name": "excavator", //n
				"data": "040e7ad1f567faf8",
				"fg": "#980",
				"bg": "#ddf"
			},
			{
				"name": "lightning", //n
				"data": "3c1c0e7e30180c04",
				"fg": "#fe2",
				"bg": "#113"
			},
			{
				"name": "saturn", //n
				"data": "001824c3ff241800",
				"fg": "#fa4",
				"bg": "#222"
			},
			{
				"name": "vacuum cleaner", //n
				"data": "6020307070703c3e",
				"fg": "#525",
				"bg": "#fda"
			},
			{
				"name": "microphone", //n
				"data": "18185a5a663c183c",
				"fg": "#222",
				"bg": "#ca7"
			}
		],
		[
			{
				"name": "pacifier", //n
				"data": "183c3c187e242418",
				"fg": "#962",
				"bg": "#fea"
			},
			{
				"name": "elephant", //n
				"data": "0060feffffbfb6b6",
				"fg": "#666",
				"bg": "#faa"
			},
			{
				"name": "yo-yo", //n
				"data": "bfcff7f7cb8585cb",
				"fg": "#9ed",
				"bg": "#e11"
			},
			{
				"name": "monkey wrench", //n
				"data": "1c10103c1c0c0c0c",
				"fg": "#811",
				"bg": "#cbd"
			},
			{
				"name": "movie camera", //n
				"data": "18dcfcf97f7f79e8",
				"fg": "#222",
				"bg": "#fa5"
			},
			{
				"name": "doughnut", //n
				"data": "007e8199c3ff7e00",
				"fg": "#852",
				"bg": "#fbe"
			},
			{
				"name": "teapot", //n
				"data": "103838fbbabefc38",
				"fg": "#534",
				"bg": "#fed"
			},
			{
				"name": "evergreen", //n
				"data": "080c180e3c1f7e08",
				"fg": "#251",
				"bg": "#aab"
			}
		],
		[
			{
				"name": "ladle", //n
				"data": "6c28242c203c2418",
				"fg": "#444",
				"bg": "#dca"
			},
			{
				"name": "magnet", //n
				"data": "e7a5a5e7e7ff7e3c",
				"fg": "#511",
				"bg": "#aaa"
			},
			{
				"name": "screw", //n
				"data": "3e1c180c180c1808",
				"fg": "#222",
				"bg": "#dc6"
			},
			{
				"name": "alarm clock", //n
				"data": "00367f6b3a221c36",
				"fg": "#c11",
				"bg": "#bba"
			},
			{
				"name": "magnifying glass", //n
				"data": "3c6642663c181818",
				"fg": "#444",
				"bg": "#dc7"
			},
			{
				"name": "syringe", //n, e
				"data": "00043dcf3d040000",
				"fg": "#222",
				"bg": "#ada"
			},
			{
				"name": "balloon", //n, h
				"data": "1c3a3e3e1c081810",
				"fg": "#e22",
				"bg": "#abf"
			},
			{
				"name": "trampoline", //n
				"data": "081c081400ff8181",
				"fg": "#222",
				"bg": "#9cf"
			}
		],
		[
			{
				"name": "snowflake", //n
				"data": "7edb99e7e799db7e",
				"fg": "#eee",
				"bg": "#99f"
			},
			{
				"name": "penny", //n
				"data": "c3991c3c181cbdc3",
				"fg": "#842",
				"bg": "#fb5"
			},
			{
				"name": "helicopter", //n, e
				"data": "fe107abffa287c00",
				"fg": "#222",
				"bg": "#88f"
			},
			{
				"name": "cube", //n,e
				"data": "fcc2ffe1e1e1613f",
				"fg": "#333",
				"bg": "#afa"
			},
			{
				"name": "paw print", //n, e
				"data": "001455411c3e3e36",
				"fg": "#852",
				"bg": "#eee"
			},
			{
				"name": "clock", //n
				"data": "1c3677677f3e1c00",
				"fg": "#222",
				"bg": "#ade"
			},
			{
				"name": "scissors", //e, n
				"data": "0808e8bfc80c141c",
				"fg": "#222",
				"bg": "#ddd"
			},
			{
				"name": "train", //n, h
				"data": "07f2a2bee3fefecb",
				"fg": "#433",
				"bg": "#cde"
			}
		],
		[
			{
				"name": "flask", //n, e
				"data": "3c181818347aff7e",
				"fg": "#633",
				"bg": "#ffb"
			},
			{
				"name": "bow and arrow", //n
				"data": "1c64bbd5ed961abc",
				"fg": "#53c",
				"bg": "#dc8"
			},
			{
				"name": "surf wave", //n
				"data": "30785c0c0e1e3fff",
				"fg": "#06a",
				"bg": "#abf"
			},
			{
				"name": "duct tape", //n
				"data": "78dc8eaeae8ede77",
				"fg": "#777",
				"bg": "#fed"
			},
			{
				"name": "bone", //n
				"data": "f9f8f0e3c70f1f9f",
				"fg": "#222",
				"bg": "#a98"
			},
			{
				"name": "squirrel", //n
				"data": "60f43667ccde781c",
				"fg": "#741",
				"bg": "#9c9"
			},
			{
				"name": "kettle", //n
				"data": "1e0309dd7f3f3f3f",
				"fg": "#222",
				"bg": "#fad"
			},
			{
				"name": "crown", //n, e (final?)
				"data": "001899dbffff0000",
				"fg": "#c80",
				"bg": "#436"
			}
		],
		/* normal set 2 */
		[
			{
				"name":"smile", //n
				"data":"3e6b6b7f5d633e00",
				"fg": "#db4",
				"bg": "#444"
			},
			{
				"name":"stethoscope", //n
				"data":"1b11111b0e04dcf0",
				"fg": "#222",
				"bg": "#ac8"
			},
			{
				"name":"computer mouse", //n
				"data":"08081c2a3e22221c",
				"fg": "#222",
				"bg": "#ccc"
			},
			{
				"name":"butterfly", //n
				"data":"147749496b2a3600",
				"fg": "#831",
				"bg": "#fd2"
			},
			{
				"name":"mitten", //n
				"data":"70f8f8fbffff7e7c",
				"fg": "#922",
				"bg": "#8cf"
			},
			{
				"data":"000702fea0b4ff00", //n
				"name":"telegraph",
				"fg": "#530",
				"bg": "#edc"
			},
			{
				"name":"whistle", //n
				"data":"007fdfdff8f87000",
				"fg": "#623",
				"bg": "#5af"
			},
			{
				"name":"bowling pin", //n
				"data":"0814141c2222361c",
				"fg": "#611",
				"bg": "#eee"
			}
		],
		[
			{
				"name":"open door", //n, h
				"data":"3e3a323232321418",
				"fg": "#531",
				"bg": "#fda"
			},
			{
				"name":"price tag", //h, n
				"data":"0321703262702103",
				"fg": "#226",
				"bg": "#fe3"
			},
			{
				"name":"fire extinguisher", //n
				"data":"3c681c141414141c",
				"fg": "#222",
				"bg": "#e33"
			},
			{
				"data":"321e143c2e080808", //n
				"name":"pinwheel",
				"fg": "#726",
				"bg": "#acf"
			},
			{
				"name":"floppy disk", //n
				"data":"ffa5a5bd81bdb5fe",
				"fg": "#222",
				"bg": "#7ae"
			},
			{
				"name":"corn", //n
				"data":"1834a5f766763c38",
				"fg": "#270",
				"bg": "#fe2"
			},
			{
				"name":"harp", //n
				"data":"0e1e64442434143e",
				"fg": "#531",
				"bg": "#9f9"
			},
			{
				"name":"oil can", //n
				"data":"000f08789cbebe3e",
				"fg": "#222",
				"bg": "#8bf"
			}
		],
		[
			{
				"name":"ant face", //n
				"data":"42247edbffff4266",
				"fg": "#222",
				"bg": "#dc9"
			},
			{
				"name":"square root", //n
				"data":"e0e03037371c1c08",
				"fg": "#113",
				"bg": "#eee"
			},
			{
				"name":"hamburger", //n
				"data":"007e9581ffff817e",
				"fg": "#421",
				"bg": "#fda"
			},
			{
				"name":"frog", //n
				"data":"245a7e7ec3e77eff",
				"fg": "#262",
				"bg": "#ed3"
			},
			{
				"name":"treadmill", //n
				"data":"3030e1bb21f199ff",
				"fg": "#223",
				"bg": "#ada"
			},
			{
				"name":"faucet", //n
				"data":"7c101038feff3b03",
				"fg": "#222",
				"bg": "#8cf"
			},
			{
				"name":"wristwatch", //n, h
				"data":"1c1c1c2a6e221c1c",
				"fg": "#222",
				"bg": "#fed"
			},
			{
				"name":"gas pump", //n
				"data":"0f69c99fafafaf4f",
				"fg": "#622",
				"bg": "#bbb"
			}
		],
		[
			{
				"name":"broccoli", //n
				"data":"6cffdf6a3e1c1818",
				"fg": "#270",
				"bg": "#eee"
			},
			{
				"name":"wishbone", //n
				"data":"9cddddddc9e3f7f7",
				"fg": "#321",
				"bg": "#eca"
			},
			{
				"name":"carrot", //n
				"data":"7edb24243414140c",
				"fg": "#251",
				"bg": "#ea3"
			},
			{
				"name":"chip and dip", //n
				"data":"00e090deff81663c",
				"fg": "#711",
				"bg": "#fc6"
			},
			{
				"data":"301c067e427e7e7e", //n
				"name":"clapperboard",
				"fg": "#222",
				"bg": "#fed"
			},
			{
				"name":"mouse", //n
				"data":"00045771fd3f1f3e",
				"fg": "#666",
				"bg": "#fed"
			},
			{
				"name":"swan", //n, h
				"data":"060f080ce67b7f3e",
				"fg": "#eee",
				"bg": "#28f"
			},
			{
				"name":"ampersand", //h, n
				"data":"1c36361cd66373de",
				"fg": "#717",
				"bg": "#afa"
			}
		],
		[
			{
				/* Jacob Cappell */
				"name": "scream", //n, h
				"data": "387c54fdeda9db5a",
				"fg": "#cdc",
				"bg": "#448"
			},
			{
				"name":"sumo wrestler", //n, h
				"data":"081c36415d2a4977",
				"fg": "#531",
				"bg": "#fda"
			},
			{
				"name":"galaxy", //n
				"data":"001e33399ccc7800",
				"fg": "#9cf",
				"bg": "#222"
			},
			{
				"name":"t-shirt", //n, h
				"data":"e79981c34242427e",
				"fg": "#030",
				"bg": "#5e5"
			},
			{
				"name":"baby carriage", //h, n
				"data":"0e0a09ff7f3c6666",
				"fg": "#311",
				"bg": "#abf"
			},
			{
				"name":"blowtorch", //n
				"data":"0ef14ee0e0e0e0e0",
				"fg": "#500",
				"bg": "#fed"
			},
			{
				"data":"0c123a3e7c7c7c38", //n, e
				"name":"eggplant",
				"fg": "#427",
				"bg": "#9f7"
			},
			{
				"name":"pruning shears", //h, n
				"data":"6c36361c36636363",
				"fg": "#700",
				"bg": "#9ae"
			}
		],
		[
			{
				"name":"tractor", //n, h
				"data":"1e5252fefb71dbce",
				"fg": "#060",
				"bg": "#fe3"
			},
			{
				"name":"robot arm", //n
				"data":"e03fe30206063f3f",
				"fg": "#600",
				"bg": "#ccc"
			},
			{
				"name":"gas mask", //n
				"data":"3e7f49493edcfefe",
				"fg": "#444",
				"bg": "#dda"
			},
			{
				"name":"tape dispenser", //n
				"data":"000e1f9bdfff7e00",
				"fg": "#333",
				"bg": "#5a5"
			},
			{
				"name":"pumpkin", //n
				"data":"0c187ea5a5a5e77e",
				"fg": "#281",
				"bg": "#fa4"
			},
			{
				"name":"clamp", //n, h
				"data":"3e033b13133e107c",
				"fg": "#800",
				"bg": "#ccc"
			},
			{
				"name":"electric guitar", //n
				"data":"3010145c783c7c38",
				"fg": "#82b",
				"bg": "#8f8"
			},
			{
				"name":"chainsaw", //n
				"data":"001070bfb1fe0000",
				"fg": "#222",
				"bg": "#ba8"
			}
		],
		[
			{
				"name":"knight", //n
				"data":"387c7e76783c7e7e",
				"fg": "#222",
				"bg": "#ccc"
			},
			{
				"name":"picnic basket", //n
				"data":"181818ffff42427e",
				"fg": "#631",
				"bg": "#59f"
			},
			{
				"name":"witch hat", //n
				"data":"101808181c3c24ff",
				"fg": "#405",
				"bg": "#ba9"
			},
			{
				"name":"stovetop", //n
				"data":"066f6f0660f6f660",
				"fg": "#222",
				"bg": "#fed"
			},
			{
				"name":"parthenon", //n
				"data":"f7c180d5d5d580ff",
				"fg": "#48f",
				"bg": "#eee"
			},
			{
				"name":"tent", //n
				"data":"387462ebc9c9c9ff",
				"fg": "#620",
				"bg": "#eee"
			},
			{
				"name":"lollipop", //n
				"data":"1c323a2e1c080808",
				"fg": "#904",
				"bg": "#eee"
			},
			{
				"name":"open book", //n
				"data":"605f49494969597f",
				"fg": "#321",
				"bg": "#fed"
			}
		],
		[
			{
				"name":"mountain", //n
				"data":"38244692fbffffff",
				"fg": "#769",
				"bg": "#eee"
			},
			{
				"name":"sombrero", //n
				"data":"00182424ff817e00",
				"fg": "#531",
				"bg": "#fda"
			},
			{
				"data":"00f060f2f9ff7e00", //n, h
				"name":"elf shoe",
				"fg": "#283",
				"bg": "#fd3"
			},
			{
				"name":"juggler", //h, n
				"data":"181bc3d8194f7c0c",
				"fg": "#611",
				"bg": "#e8f"
			},
			{
				"name":"tuba", //h, n
				"data":"3f1e6cbcfebcdc78",
				"fg": "#530",
				"bg": "#fc5"
			},
			{
				"name":"crowbar", //h, n
				"data":"387cecae06060705",
				"fg": "#222",
				"bg": "#fe3"
			},
			{
				"data":"ff7fbf5700a341ff", //n (last set2?)
				"image": "images/puzzle/prism.png",
				"name":"prism",
				"fg": "#000",
				"bg": "#fff"
			},
			{
				"name":"medal", //n (last)
				"data":"7ec36624183c3c18",
				"fg": "#a70",
				"bg": "#9bf"
			}
		]
	],


	"hard": [
		[
			{
				"name": "power", //h (first?)
				"data": "082a494141221c00",
				//"data": "082a6b4141633e00",
				"fg": "#188",
				"bg": "#eee"
			},
			{
				"name": "diamond", //h
				"data": "001c227d32140800",
				"fg": "#558",
				"bg": "#aaf"
			},
			{
				"name": "two cents", //h
				"data": "040f054ff450f040",
				"fg": "#765",
				"bg": "#afa"
			},
			{
				"name": "strawberry", //h
				"data": "207c72e65265390e",
				"fg": "#040",
				"bg": "#f36"
			},
			{
				"name": "bread slice", //n, h
				"data": "7e8181c34242427e",
				"fg": "#531",
				"bg": "#ea6"
			},
			{
				"name": "drop", //h, n
				"data": "101834347e5e2418",
				"fg": "#349",
				"bg": "#eee"
			},
			{
				"name": "penduin", //n, h
				"data": "181e18343474183c",
				"fg": "#222",
				"bg": "#eee"
			},
			{
				"name": "flamingo", //h
				"data": "2070501c0e04040c",
				"fg": "#b26",
				"bg": "#ddf"
			}
		],
		[
			{
				"name": "baby", //h
				"data": "00c0cefa3a2c6f00",
				"fg": "#531",
				"bg": "#fed"
			},
			{
				"name": "pipe", //h
				"data": "0007ece8f8787000",
				"fg": "#432",
				"bg": "#a95"
			},
			{
				"name": "ufo", //n, h
				"data": "001824247ed5ff00",
				"fg": "#444",
				"bg": "#99f"
			},
			{
				"name": "scorpion", //h
				"data": "03010efc58081808",
				"fg": "#810",
				"bg": "#9a9"
			},
			{
				"name": "mailbox", //h
				"data": "707e979585fe3030",
				"fg": "#732",
				"bg": "#bbd"
			},
			{
				"name": "glasses", //h
				"data": "00cc427f49493600",
				"fg": "#511",
				"bg": "#eeb"
			},
			{
				"name": "flower", //hard
				"data": "1c362a361c306040",
				"fg": "#041",
				"bg": "#fbf"
			},
			{
				"name": "parrot", //h
				"data": "0c161e3a38787098",
				"fg": "#811",
				"bg": "#fe5"
			}
		],
		[
			{
				"name": "goldfish bowl", //h
				"data": "ff4299b99181423c",
				"fg": "#fa2",
				"bg": "#46f"
			},
			{
				"name": "tissue box", //h
				"data": "162a24143e22223e",
				"fg": "#777",
				"bg": "#ece"
			},
			{
				"name": "mask", //h
				"data": "2a1408ffc9b68080",
				"fg": "#266",
				"bg": "#dca"
			},
			{
				"name": "pretzel", //h
				"data": "00364949361c2200",
				"fg": "#531",
				"bg": "#cba"
			},
			{
				"name": "slingshot", //h
				"data": "0033559beec40404",
				"fg": "#432",
				"bg": "#aac"
			},
			{
				"name": "bow", //h
				"data": "0063774914143663",
				"fg": "#837",
				"bg": "#fec"
			},
			{
				"name": "socks",
				"data": "7c54549289c56638",
				"fg": "#321",
				"bg": "#fed"
			},
			{
				"name": "cat", //h
				"data": "0a0e0e442c2c3c1e",
				"fg": "#421",
				"bg": "#9b9"
			}
		],
		[
			{
				"name": "earth", //h
				"data": "3c5aed9d8f995a3c",
				"fg": "#118",
				"bg": "#5b4"
			},
			{
				"name": "jug", //h
				"data": "1c08785c7e32323e",
				"fg": "#621",
				"bg": "#d98"
			},
			{
				"name": "tyrannosaur", //h
				"data": "070f0c3f78f4b038",
				"fg": "#141",
				"bg": "#7ab"
			},
			{
				"name": "turtle", //h
				"data": "00ccde5e611e3300",
				"fg": "#551",
				"bg": "#aac"
			},
			{
				"name": "wheelbarrow", //n, h
				"data": "00fffc7c32cac600",
				"fg": "#822",
				"bg": "#8b8"
			},
			{
				"name": "banana", //h
				"data": "303050484864361e",
				"fg": "#532",
				"bg": "#ed4"
			},
			{
				"name": "saxophone", //h
				"data": "70d81832363f3e18",
				"fg": "#851",
				"bg": "#aea"
			},
			{
				"name": "light bulb", //h
				"data": "1c22415d492a1c1c",
				"fg": "#444",
				"bg": "#ee6"
			}
		],
		[
			{
				"name": "bubble", //h
				"data": "3c66cb8581c3663c",
				"fg": "#269",
				"bg": "#7cf"
			},
			{
				"name": "shuttlecock", //h
				"data": "18347e5ac381a5db",
				"fg": "#500",
				"bg": "#eee"
			},
			{
				"name": "candle", //n, h
				"data": "081412ccacec3f1e",
				"fg": "#765",
				"bg": "#fe8"
			},
			{
				"name": "chocolate kiss", //h
				"data": "0e0b08143263613e",
				"fg": "#777",
				"bg": "#eee"
			},
			{
				"name": "cassette tape", //h
				"data": "007e81a581bd7e00",
				"fg": "#433",
				"bg": "#fa5"
			},
			{
				"name": "fishing hook", //h
				"data": "382810101216120e",
				"fg": "#222",
				"bg": "#abf"
			},
			{
				"name": "chair", //n, h
				"data": "181e16fafeaaaa22",
				"fg": "#421",
				"bg": "#eca"
			},
			{
				"name": "peace", //n,h
				"data": "1c2a49495d2a1c00",
				"fg": "#292",
				"bg": "#ebf"
			}
		],
		[
			{
				"name": "horseshoe", //h
				"data": "1c36636363632263",
				"fg": "#321",
				"bg": "#6a6"
			},
			{
				"name": "fist", //h
				"data": "3e5555fd87b98142",
				"fg": "#420",
				"bg": "#fda"
			},
			{
				"name": "whale", //n, h
				"data": "0a15040eae4e7e00",
				"fg": "#229",
				"bg": "#aaf"
			},
			{
				"name": "dead tree", //h, n
				"data": "080c98d277fc387c",
				"fg": "#421",
				"bg": "#c5f"
			},
			{
				"name": "french horn", //h, n
				"data": "40586c446d3b1f03",
				"fg": "#751",
				"bg": "#afa"
			},
			{
				"name": "headphones", //h, n
				"data": "18244242e7e76600",
				"fg": "#222",
				"bg": "#d79"
			},
			{
				"name": "fish", //h, n
				"data": "0f3b7ffe3cf06020",
				"fg": "#131",
				"bg": "#abf"
			},
			{
				"name": "horse", //h, n
				"data": "141c362643879be9",
				"fg": "#753",
				"bg": "#ec8"
			}
		],
		[
			{
				"name": "cowboy hat", //h, m
				"data": "00243c3cbdc37e00",
				"fg": "#642",
				"bg": "#fb3"
			},
			{
				"name": "bell", //h, n
				"data": "1824246681ff2818",
				"fg": "#851",
				"bg": "#cba"
			},
			{
				"name": "shopping cart", //h, n
				"data": "03feaaaa7e02fec6",
				"fg": "#777",
				"bg": "#abc"
			},
			{
				"name": "dolphin", //h, n
				"data": "00301c3a7fd04000",
				"fg": "#278",
				"bg": "#aae"
			},
			{
				"name": "tooth", //n, h
				"data": "3e494141222a2a1c",
				"fg": "#321",
				"bg": "#feb"
			},
			{
				"name": "steering wheel", //h, n
				"data": "3c66c399ffc3663c",
				"fg": "#531",
				"bg": "#baa"
			},
			{
				"name": "reindeer", //n, h
				"data": "4155771c1c3e361c",
				"fg": "#631",
				"bg": "#eee"
			},
			{
				"name": "disguise", //n, h
				"data": "66ff99e742247ee7",
				"fg": "#222",
				"bg": "#faa"
			}
		],
		[
			{
				"name": "enyo", //h
				"data": "3c5a667a665a2418",
				"fg": "#238",
				"bg": "#eee"
			},
			{
				"name": "duck", //n, h
				"data": "3078f8307f7e3e1c",
				"fg": "#d92",
				"bg": "#dee"
			},
			{
				"name": "apple", //n, h
				"image": "images/puzzle/apple.png",
				"data": "18083e7d7f7f3e36",
				"fg": "#a01",
				"bg": "#acf"
			},
			{
				"name": "camel", //n, h
				"data": "005677fcfc90d800",
				"fg": "#753",
				"bg": "#dcb"
			},
			{
				"name": "zipper", //h, n  hmm...?
				"data": "3006181c081c141c",
				"fg": "#313",
				"bg": "#7a7"
			},
			{
				"name": "rabbit", //n, h
				"data": "3636141c2a3e771c",
				"fg": "#876",
				"bg": "#ebe"
			},
			{
				"name": "rooster", //h, n
				"data": "3366e7343e1c0818",
				"fg": "#411",
				"bg": "#fa4"
			},
			{
				"name": "star", //h
				"data": "0818ff7e3c7e66c3",
				"fg": "#fa1",
				"bg": "#c11"
			}
		],
		/* hard set 2 */
		[
			{
				"data":"003e4db3acf06000", //h (first of set2)
				"name":"brain",
				"fg": "#633",
				"bg": "#fdd"
			},
			{
				"name":"moth", //h
				"data":"3848cc9fb9f1263c",
				"fg": "#332",
				"bg": "#ccc"
			},
			{
				"data":"362a1c777736363e", //h
				"name":"gift",
				"fg": "#227",
				"bg": "#fae"
			},
			{
				"name":"cask", //h
				"data":"007884fc87fd7800",
				"fg": "#432",
				"bg": "#eca"
			},
			{
				"name":"cherries", //h
				"data":"3c18181436494936",
				"fg": "#040",
				"bg": "#f55"
			},
			{
				"name":"octopus", //h
				"data":"183c3c98fd3f74d6",
				"fg": "#524",
				"bg": "#2aa"
			},
			{
				"name":"pizza", //h, n
				"data":"3cc49cf46e361d07",
				"fg": "#922",
				"bg": "#fc3"
			},
			{
				/* Jacob Cappell */
				"name":"submarine", //h
				"data":"1810efc7002a81ff",
				"fg": "#566",
				"bg": "#acd"
			},
		],
		[
			{
				"name":"robber", //h
				"data":"1c223e6afea22c28",
				"fg": "#222",
				"bg": "#feb"
			},
			{
				"name":"chicken leg", //h
				"data":"70f0f8f876190b06",
				"fg": "#532",
				"bg": "#fda"
			},
			{
				"name":"saw blade", //h, n
				"data":"26bcff6666ff3d64",
				"fg": "#666",
				"bg": "#ade"
			},
			{
				"name":"cannon", //h
				"data":"040e1f3e6cd4c47c",
				"fg": "#222",
				"bg": "#eda"
			},
			{
				"name":"toad", //h
				"data":"3058fc9e4f2f3f6e",
				"fg": "#642",
				"bg": "#eb4"
			},
			{
				"name":"torch", //n, h
				"data":"187755413e1c0808",
				"fg": "#811",
				"bg": "#fc2"
			},
			{
				"name":"croissant", //h
				"data":"3e6947ac98b06000",
				"fg": "#420",
				"bg": "#eb6"
			},
			{
				/* Jacob Cappell */
				"name": "anteater", //h
				"data": "f8160265514d25e7",
				"fg": "#531",
				"bg": "#ec9",
			}
		],
		[
			{
				"data":"103c48483c91ff7e", //h
				"name":"viking ship",
				"fg": "#631",
				"bg": "#acf"
			},
			{
				"data":"20743c781afebf3c", //h
				"name":"coral",
				"fg": "#c29",
				"bg": "#5be"
			},
			{
				"data":"38707c585fd91818", //n, h
				"name":"water pump",
				"fg": "#522",
				"bg": "#9bf"
			},
			{
				"data":"0cc7bf84447c4848", //h
				"name":"sheep",
				"fg": "#322",
				"bg": "#edd"
			},
			{
				"name":"eye", //n, h
				"data":"ffc399343c99c3ff",
				"fg": "#222",
				"bg": "#fee"
			},
			{
				/* Jacob Cappell */
				"name": "nose", //h
				"data": "f0e0e0c08e11019a",
				"fg": "#420",
				"bg": "#fda"
			},
			{
				"data":"00363e777f777700", //n, h
				"name":"binoculars",
				"fg": "#222",
				"bg": "#9ce"
			},
			{
				"data":"3c729ba7e5d94e3c", //h
				"name":"safety ring",
				"fg": "#922",
				"bg": "#eee"
			}
		],
		[
			{
				"data":"08081c1c22361c36", //n, h
				"image": "images/puzzle/gardengnome.png",
				"name":"garden gnome",
				"fg": "#b15",
				"bg": "#eee"
			},
			{
				"data":"10282879bbbd7838", //h
				"name":"watering can",
				"fg": "#a11",
				"bg": "#9cf"
			},
			{
				"data":"eeb81c10ff3c3c3c", //n, h
				"name":"magician's hat",
				"fg": "#333",
				"bg": "#f9d"
			},
			{
				"name":"slide", //h, n
				"data":"0040e070d84cc643",
				"fg": "#444",
				"bg": "#9bf"
			},
			{
				"data":"1c3e5d496b492a1c", //h
				"name":"ladybug",
				"fg": "#222",
				"bg": "#f62"
			},
			{
				"data":"000274fffffcd848", //n, h
				"name":"pig",
				"fg": "#945",
				"bg": "#8a7"
			},
			{
				"data":"0c2d3f0c1e72d21e", //h
				"name":"jack in the box",
				"fg": "#625",
				"bg": "#fda"
			},
			{
				"name":"football", //h, n
				"data":"f0fce6f27b7f3f0f",
				"fg": "#642",
				"bg": "#eee"
			}
		],
		[
			{
				"data":"18083e6b7f55633e", //h
				"name":"jack-o-lantern",
				"fg": "#c60",
				"bg": "#fd2"
			},
			{
				"data":"107f321c0808081c", //h
				"name":"martini",
				"fg": "#242",
				"bg": "#bac"
			},
			{
				"data":"3c429991c3fd723c", //h
				"name":"compass",
				"fg": "#227",
				"bg": "#eee"
			},
			{
				"data":"105078386c464444", //h (next to other compass)
				"name":"compass",
				"fg": "#333",
				"bg": "#fda"
			},
			{
				"data":"041e0e7ffb784848", //n, h
				"name":"calf",
				"fg": "#531",
				"bg": "#8a7"
			},
			{
				"data":"3c5ac39999e7663c", //h
				"name":"soccer ball",
				"fg": "#322",
				"bg": "#eed"
			},
			{
				"data":"3c6e7b7f3dffd67c", //h
				"name":"palette",
				"fg": "#531",
				"bg": "#ead"
			},
			{
				"data":"7ec399c37effc37e", //n, h
				"name":"spool",
				"fg": "#622",
				"bg": "#fda"
			}
		],
		[
			{
				"data": "7ccef8f0ffb1f937", //h
				"name": "football helmet",
				"fg": "#170",
				"bg": "#eee"
			},
			{
				"name":"recycle", //h
				"data":"183c647007a6f366",
				"fg": "#491",
				"bg": "#eee"
			},
			{
				/* Jacob Cappell */
				"name": "dragon", //h
				"data": "9f6eac29ba591d37",
				"fg": "#4b1",
				"bg": "#400"
			},
			{
				"name":"perfume", //h
				"data":"367f364884844878",
				"fg": "#928",
				"bg": "#fcc"
			},
			{
				"name":"dna", //h
				"data":"180c142830180c14",
				"fg": "#576",
				"bg": "#abc"
			},
			{
				"name":"peeled banana", //h
				"data":"0814147699e54bf0",
				"fg": "#532",
				"bg": "#ed4"
			},
			{
				/* Jacob Cappell */
				"name": "flying bird", //h
				"data": "0f6cf838ffc68480",
				"fg": "#222",
				"bg": "#8af"
			},
			{
				"name":"jellyfish", //h
				"data":"c3bd7e00a5a56a5b",
				"fg": "#036",
				"bg": "#def"
			}
		],
		[
			{
				"name":"volcano", //h
				"data":"282c6cd6d6b33b5d",
				"fg": "#531",
				"bg": "#f41"
			},
			{
				"name":"ice auger", //h
				"data":"1c080e020e180c08",
				"fg": "#222",
				"bg": "#bbf"
			},
			{
				"name":"mermaid", //h
				"data":"1c36574135de7cf0",
				"fg": "#252",
				"bg": "#fda"
			},
			{
				"name":"wolf", //h
				"data":"4060b870582c3d6f",
				"fg": "#555",
				"bg": "#bbb"
			},
			{
				"name":"apatosaur", //h
				"data":"c0404040587c3e2b",
				"fg": "#643",
				"bg": "#ada"
			},
			{
				"name":"army man toy", //h, n
				"data":"3030187c382c64fe",
				"fg": "#252",
				"bg": "#ccc"
			},
			{
				"name":"jigsaw puzzle", //h, n
				"data":"1a0f1a40eb4eeb40",
				"fg": "#543",
				"bg": "#fed"
			},
			{
				/* Jacob Cappell */
				"name":"acorn", //h, n
				"data":"dc3266495d3f0f00",
				"fg": "#531",
				"bg": "#fda"
			}
		],
		[
			{
				"name":"paper fan", //h, n
				"data":"e0f8fcbeaeffe7ff",
				"fg": "#753",
				"bg": "#abf"
			},
			{
				"name":"bicyclist", //h, n
				"data":"063e786cf6bd9966",
				"fg": "#222",
				"bg": "#58f"
			},
			{
				"name":"rocking horse", //h, n
				"data":"04060f7c7c24e77e",
				"fg": "#531",
				"bg": "#faa"
			},
			{
				/* Jacob Cappell */
				"name": "tornado", //h
				"data": "c1f9321e0c081010",
				"fg": "#565",
				"bg": "#bbc"
			},
			{
				"name":"metal detector", //h, n
				"data":"f0b038101808081f",
				"fg": "#333",
				"bg": "#ca8"
			},
			{
				"name":"megaphone", //h, n
				"data":"01037df1fd636170",
				"fg": "#900",
				"bg": "#eee"
			},
			{

				"name":"pie", //h, n
				"data":"3868c482f3fd3f07",
				"fg": "#702",
				"bg": "#ec8"
			},
			{
				"name":"omega", //n
				"data":"3c66c3c3c36624e7",
				"fg": "#311",
				"bg": "#eee"
			}
		]
	],

	"extra": [
		[
			{
				"name":"pellican", //n, h
				"data":"18387cec6e0e071c",
				"fg": "#222",
				"bg": "#eee"
			},
			{
				"name": "up", //e
				"data": "081c3e7f1c1c1c1c",
				"fg": "#228",
				"bg": "#eef"
			},
			{
				"name":"pants", //e
				"data":"3e3e3e3636363636",
				"fg": "#117",
				"bg": "#ccc"
			},
			{
				"name":"laptop", //e
				"data":"007e4242427eff00",
				"fg": "#222",
				"bg": "#5fa"
			},
			{
				"name":"clam", //h
				"data":"186681abffd5827c",
				"fg": "#226",
				"bg": "#eee"
			},
			{
				"name":"cowbell", //e
				"data":"3c243c3c7e7e7e7e",
				"fg": "#830",
				"bg": "#3ea"
			},
			{
				"name":"hockey stick", //n
				"data":"4040602030101e0e",
				"fg": "#333",
				"bg": "#8af"
			},
			{
				"name":"movie ticket", //e
				"data":"00ff814281ff0000",
				"fg": "#431",
				"bg": "#fda"
			},
			{
				"name":"cloud", //h
				"data":"ffe7c3a7090091ff",
				"fg": "#22f",
				"bg": "#eee"
			}
		],
		[
			/* Adam Marks */
			{
				"name":"Superman",
				"data":"ff81b9e34e5a2418"
			},
			{
				"name":"Holy hand grenade of antioch",
				"data":"081c081c3e363e1c"
			},
			{
				"name":"Pac man",
				"data":"0c1e3b0fa70f3f1e"
			},
			{
				"name":"Elephant",
				"data":"77777f3e1c082838"
			},
			{
				"name":"Mickey mouse",
				"data":"e7e7e73c6642663c"
			},
			{
				"name":"Superman v2",
				"data":"ff819989525a2418"
			},
			{
				"name":"Hangman",
				"data":"1f1139117d11292b"
			},
			{
				"name":"NY Yankees",
				"data":"446cba969ebaf2d2"
			},
			{
				"name":"Spider",
				"data":"99db7e3cff3c5ac3"
			},
			{
				"name":"Clown face",
				"data":"0022772a1c22221c"
			},
			{
				"name":"hp",
				"data":"000202eeaaea2020"
			},
			{
				"name":"Checkboard",
				"data":"9966669999666699"
			}
		],

		[
			/* Jacob Cappell */
			{
				"name":"camel",
				"data":"03022a7e7e5c4444"
			},
			{
				"name":"venom",
				"data":"7ebd99ffbd42663c"
			},
			{
				"name":"snowflake","data":"ab36d5e007ab6cd5"
			},
			{
				"name":"Jack-o-Lantern",
				"data":"30087edbffa5cb7e"
			},
			{
				"name":"submarine",
				"data":"1810efc7002a81ff"
			},
			{
				"name":"taxi",
				"data":"18187e42ffbdff81"
			},
			{
				"name":"bird",
				"data":"0f6cf838ffce8480"
			},
			{
				"name":"ambulance",
				"data":"103f45af8581ff66"
			},
			{
				"name":"ant",
				"data":"80404cdeffbb544a"
			},
			{
				"name":"stegasaur",
				"data":"0028aa7cfffda440"
			},
			{
				"name":"ladybug",
				"data":"38ba7c92d692d67c"
			},
			{
				"name":"android mascot",
				"data":"38557dfffcfc7c28"
			},
			{
				"name":"Puppeteer",
				"data":"332211157efe8a8a"
			},
			{
				"name":"butterfly",
				"data":"24dbbd99995aa5c3"
			},
			{
				"name":"Nose",
				"data":"f0e0e0c08e11019a"
			},
			{
				"name":"knight",
				"data":"3e7f767a7c7c387c"
			},
			{
				"name":"frog","data":"3c046f9db9350418"},
			{"name":"Calvin","data":"00009280001e0d83"},
			{"name":"Hobbes","data":"3c817e81a5008118"},
			{"name":"racecar","data":"f8fdc0a60916f6f9"},
			{"name":"B-2","data":"ff9fbcf4e8f0c0c0"},
			{"name":"The Moon","data":"813a7ba5430f0c81"},
			{"name":"Jolly Roger","data":"e3d5c1a21ce31cbe"},
			{"name":"T.A.R.D.I.S.","data":"3c7e24243c3c3c7e"},
			{"name":"Enterprise","data":"ff0dbfa0b70efbdf"},
			{"name":"tornado","data":"c1fb721e0c081010"},
			{"name":"R2","data":"1834bdffbdbd99bd"},
			{"name":"dragon","data":"9f6eac29ba591d32"},
			{"name":"evil eye","data":"2147997993c6bc00"},
			{"name":"Homer","data":"a30749abc986d920"},
			{"name":"Mario","data":"4f4b13f783fe52a1"},
			{"name":"Moai Statue","data":"8181ff89cbdbc35a"},
			{"name":"Mona Lisa","data":"7ee1c0db8282d0ce"},
			{"name":"Venus of Willendorf","data":"1c223e4977412a1c"},
			{"name":"Anteater","data":"f82804caa29a8ace"},
			{"name":"Acorn","data":"dc3266495d3f0f00"},
			{"name":"Diving Board","data":"a0fea0cf90af5fbf"},
			{"name":"Lion","data":"5cce8faf8148e35e"},
			{"name":"The Scream","data":"387c54fdeda9db5a"},
			{"name":"Elvis","data":"e08abb80c4c0cc40"},
			{"name":"Great Wall of China","data":"3b3c28341a32c60e"},
			{"name":"Eiffel Tower","data":"0808081c143e3663"},
			{"name":"Waiter & platter","data":"0f021a1a167cb878"},
			{"name":"Triceratops","data":"289464ae99e12d36"},
			{"name":"pterosaur","data":"9f8cfe9fbeb93020"},
			{"name":"Maneki Neko","data":"4a7d4f4db58684fc"},
			{"name":"rain cloud","data":"30cc920101ff9249"},
			{"name":"Koala","data":"3c8100ab89037f12"},
			{"name":"observatory","data":"dffbeebfd3b1b181"},
			{"name":"paper roll","data":"ffc399a1998989cb"},
			{"name":"water tower","data":"081c3e22223e2a49"},
			{"name":"Ouroboros","data":"1e0577c79d81c37e"},
			{"name":"microwave oven","data":"00ffa1e1edff0000"},
			{"name":"roots","data":"2424c793b91a537d"},
			{"name":"orange slice","data":"7ea395f99fa9c57e"},
			{"name":"log","data":"73aed4eef175311e"},
			{"name":"checkers","data":"077d57eab5ea576b"},
			{"name":"Go","data":"70fe7222a7fda722"},
			{"name":"clown face","data":"42e742183c99c37e"},
			{"name":"skier","data":"30381c3f0c188c7f"},
			{"name":"Mondrian Composition II","data":"0404070404ff47c7"},
			{"name":"Sherlock Holmes","data":"0f12bf139138af44"},
			{"name":"boomerang","data":"7e85bea0a0e0a0c0"},
			{"name":"mp3 player","data":"7f415d5d41495549"}
		]
	],

	"broken": [
		[
			{
				"name": "comet", //hard
				"data": "a0d0683c3e1f0f06",
				"fg": "#eff",
				"bg": "#116"
			},
			{
				"name": "pick axe", //h
				"data": "701c0e1b3161c080",
				"fg": "#311",
				"bg": "#887"
			},
			{
				"name": "treble clef", //h
				"data": "0305264c5464d8c0",
				"fg": "#222",
				"bg": "#fee"
			},
			{
				"name": "battle axe", //n
				"data": "6b5d495d6b080808",
				"fg": "#222",
				"bg": "#aaa"
			},
			{
				"name": "poison", //n, h
				"data": "c36624007edb9918",
				"fg": "#403",
				"bg": "#afa"
			},
			{
				"name": "male", //h
				"data": "e0c0a01c2222221c",
				"fg": "#521",
				"bg": "#ddf"
			},
			{
				"name": "female", //h, n
				"data": "1c2222221c081c08",
				"fg": "#521",
				"bg": "#fcf"
			}
		]
	]
};

// crop puzzle tables for demo
if(typeof(FULL) === "undefined" || !FULL) {
	for(key in PUZZLES) {
		PUZZLES[key] = PUZZLES[key].filter(function(item, index) {
			return index < 2;
		});
	}
/*
} else {
	for(key in PUZZLES) {
		PUZZLES[key] = PUZZLES[key].filter(function(item, index) {
			return index < 8;
		});
	}
*/
}

PUZZLES.custom = Storage.get("picross-custom") || [];

// trim bloated custom puzzle data
if(!enyo.platform.webos || enyo.platform.webos > 1) {
	try {
		var trimmed = [];
		var row = [];
		var obj = {};
		for(y in PUZZLES.custom) {
			row = [];
			for(x in PUZZLES.custom[y]) {
				obj = {};
				if(PUZZLES.custom[y][x].data) {
					obj.data = PUZZLES.custom[y][x].data;
					obj.name = PUZZLES.custom[y][x].name || "";
				}
				row.push(obj);
			}
			trimmed.push(row);
		}
		Storage.set("picross-custom", trimmed);
	} catch(e) {
		enyo.log("PUZZLES.custom: trim fail");
	}
}
